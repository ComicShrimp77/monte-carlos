import matplotlib.pyplot as plt
from random import random

dentro = 0
interacoes = int(input("Quantas interações? : "))

x_dentro = []
y_dentro = []
x_fora = []
y_fora = []

for _ in range(interacoes):
    x = 1-2*random()
    y = 1-2*random()
    if x**2+y**2 <= 1:
        dentro += 1
        x_dentro.append(x)
        y_dentro.append(y)
    else:
        x_fora.append(x)
        y_fora.append(y)

pi = 4*dentro/interacoes
print("Pi = ",float(pi))

fig, ax = plt.subplots(figsize = (50,20))
ax.set_aspect('equal')
ax.scatter(x_dentro, y_dentro, color='g', marker='s')
ax.scatter(x_fora, y_fora, color='r', marker='s')
plt.show()